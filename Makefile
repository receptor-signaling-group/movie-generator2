SRC_DIR := src
OBJ_DIR := obj
BIN_DIR := bin
SCRIPT_DIR := scripts
SRC_FILES := $(wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))
SCRIPTS := $(wildcard $(SCRIPT_DIR)/*.cpp)
BINARIES:= $(patsubst $(SCRIPT_DIR)/%.cpp,%,$(SCRIPTS))
BINS := $(patsubst %,$(BIN_DIR)/%, $(BINARIES))
LDFLAGS := -lgsl -lm -lopenblas -ltiff
CPPFLAGS := -O3 

all: $(BINS)

$(BIN_DIR)/%:  $(SCRIPT_DIR)/%.cpp $(OBJ_FILES)
	@mkdir -p $(@D)
	@echo "building $@"
	g++ $< -o $@ $(CPPFLAGS) $(OBJ_FILES) $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@mkdir -p $(@D)
	g++ $(CPPFLAGS) -c -o $@ $<
	
clean:
	@echo "removing all object and binary files"
	rm -rf bin/
	rm -rf obj/
