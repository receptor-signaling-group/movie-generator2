#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <vector>

#include <list>
#include <tuple>

#include "../src/image.hpp"
#include "../src/membrane.hpp"
#include "../src/tubule.hpp"


void allocate(const unsigned int m, double ** pos, gsl_rng ** rng, unsigned int seed=17835)
{
	*pos=(double *)malloc(3*m*sizeof(double));
	*rng=gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(*rng,seed);
}

void freespace(double *pos, gsl_rng *rng)
{
	free(pos);
	free(rng);
}

int main(int argc, char ** argv)
{
	
	/*constants in real units (µm,s)*/
	const double pixel_in_mu=0.05;
	const double frames_in_s=1/1800.;
	
	const double D_in_mu2_over_s = 0.1;
	const double R_in_mu=.33;
	const double Rz_in_mu=1.12;
	const double Rt_in_mu=.175;
	
	
	
	/*image settings*/
	const unsigned int numframes=1000; //number of frames
	const unsigned int iX=200; //width
	const unsigned int iY=1000; //height
	

	
	/* transform into simulation units (frames, pixels)*/	
	const double D=D_in_mu2_over_s/(pixel_in_mu*pixel_in_mu)*frames_in_s;
	const double R=R_in_mu/(pixel_in_mu);
	const double Rz=Rz_in_mu/pixel_in_mu;
	const double Rt=Rt_in_mu/pixel_in_mu;
	const double R2=R*R;
	const double Rz2=Rz*Rz;
	
	

	const double H=20*R;
	const double L=2*iX;
	const unsigned int m=180;
	
	double * pos;
	gsl_rng * rng;
	
	allocate(m, &pos,&rng);
	
	
	// Start of simulation
	

	// generate a plane (rectangle with periodic b.c.) with 180 particles diffusing with constant D
	// next three arguments: lower left corner
	// next three arguments: normal vector of plane n
	// next three arguments: vector defining what right means v, other in-plane vector is then n x v
	// next two arguments: extensions of rectangle in both planar directions
	// start of positions
	// random number source
	membrane M(180,D, -L/4,0,-L/4. ,0.,-1.,0. ,1.,0.,0., L,L, pos, rng);
	
	//imaging object, given image dimensions, in-plane and out-of-plane waist, random number source (for noise) 
	image I(iX,iY,R2,Rz2,rng);
	
	//time (in frames, we advance one frame each step)
	const double dt=1;
	double t=0;
	
	unsigned int frames=0;

	while(1)
	{
		t+=dt;
		
		//integrate equation of motions on all objects, could be nicer with nicer interface
		M.integrate(dt);
		
		// linescan, always in z=0 plane
		// first two arguments: starting point (x,y)
		// then scanning direction
		// then number of particles and array of positions
		// frames is used for file-naming
		frames+=I.linescan(0., 0., 1., 0.,m,pos,frames);
		
		if(frames==numframes) break;		
	}
	
	freespace(pos,rng);
}
