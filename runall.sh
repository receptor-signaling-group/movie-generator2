#!/bin/bash

mkdir -p workspace
make


for i in bin/*
do
    name=${i##*/}
    echo "$name"
    mkdir -p workspace/${name}
    cp "$i" workspace/${name}
    cd workspace/${name}
    ./${name}
    rm -f ${name}
    cd ../..
done
