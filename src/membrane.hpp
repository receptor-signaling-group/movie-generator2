#ifndef H_MEMBRANE

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <math.h>
#include <stdio.h>

class membrane
{
		private:
		
		double D;
		double L1,L2;
		double x,y,z;
		double nx,ny,nz;
		
		double v1x,v1y,v1z;
		double v2x,v2y,v2z;
		
		double * pos;
		unsigned int m;
		gsl_rng *rng;
		
		public:
		
		membrane(const unsigned int _m, const double _D, const double _x, const double _y, const double _z, const double _nx, const double _ny, const double _nz,
		         const double _vx, const double _vy, const double _vz, const double _L1, const double _L2, double * _pos, gsl_rng * _rng) :
		m(_m), D(_D), x(_x), y(_y), z(_z), nx(_nx), ny(_ny), nz(_nz), v1x(_vx),v1y(_vy),v1z(_vz), v2x(ny*v1z-nz*v1y),v2y(nz*v1x-nx*v1z),v2z(nx*v1y-ny*v1x), L1(_L1), L2(_L2), pos(_pos), rng(_rng)
		{	
			initialize();
		};
		void initialize();
		void integrate(const double dt);
		~membrane(){};

};

#endif
