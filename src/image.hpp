#ifndef H_IMAGE
#include <tiffio.h>
#include <stdlib.h>
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>


class image
{
	private:
	
	unsigned int row;
	const unsigned int iX;
	const unsigned int iY;
	double * pic_buffer;
	
	gsl_rng *rng;
	
	const double R2;
	const double Rz2;

	
	void buffer_to_image(const char * name);
	double compute_pixel(const double x, const double y, const unsigned int m, double *pos);
	
	public:
	image(const unsigned int width, const unsigned int height, const double _R2, const double _Rz2, gsl_rng * _rng) : iX(width), iY(height), pic_buffer((double *)malloc(iX*iY*sizeof(double))),row(0), R2(_R2), Rz2(_Rz2), rng(_rng)
	{
		
	};
	bool linescan(const double x, const double y, const double vx, const double vy, const unsigned int m, double *pos, const unsigned int frame);
	
	~image()
	{
		free(pic_buffer);
	}
};


#endif
