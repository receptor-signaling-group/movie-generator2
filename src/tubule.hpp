#ifndef H_TUBULE

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <math.h>
#include <stdio.h>

class tubule
{
		private:
		
		double D1,D2;
		double L,R;
		double x,y,z;
		double nx,ny,nz;
		
		double v1x,v1y,v1z;
		double v2x,v2y,v2z;
		
		double * pos;
		unsigned int m;
		double * phi;
		gsl_rng *rng;
		
		public:
		
		tubule(const unsigned int _m, const double _D, const double _x, const double _y, const double _z, const double _nx, const double _ny, const double _nz,
		       const double _vx, const double _vy, const double _vz, const double _L, const double _R, double * _pos, gsl_rng * _rng) :
		m(_m), D1(_D), D2(_D), x(_x), y(_y), z(_z), nx(_nx), ny(_ny), nz(_nz), v1x(_vx),v1y(_vy),v1z(_vz), v2x(ny*v1z-nz*v1y),v2y(nz*v1x-nx*v1z),v2z(nx*v1y-ny*v1x),
		L(_L), R(_R), pos(_pos), phi((double *)malloc(m*sizeof(double))), rng(_rng)
		{	
			initialize();
		};
		tubule(const unsigned int _m, const double _D1, const double _D2,const double _x, const double _y, const double _z, const double _nx, const double _ny, const double _nz, const double _vx, const double _vy, const double _vz, const double _L, const double _R, double * _pos, gsl_rng * _rng) :
		m(_m), D1(_D1), D2(_D2), x(_x), y(_y), z(_z), nx(_nx), ny(_ny), nz(_nz), v1x(_vx),v1y(_vy),v1z(_vz), v2x(ny*v1z-nz*v1y),v2y(nz*v1x-nx*v1z),v2z(nx*v1y-ny*v1x),
		L(_L), R(_R), pos(_pos), phi((double *)malloc(m*sizeof(double))), rng(_rng)
		{	
			initialize();
		};
		void initialize();
		void integrate(const double dt);
		~tubule(){free(phi);};

};

#endif

