#include "tubule.hpp"


void tubule::initialize() 
{
	for(unsigned int i=0; i<m; ++i)
	{
		const double h=L*gsl_rng_uniform(rng);
		const double p=2*M_PI*gsl_rng_uniform(rng);
		
		const double x1=R*cos(p);
		const double x2=R*sin(p);
		
		const double _x=x+x1*v1x+x2*v2x+h*nx;
		const double _y=y+x1*v1y+x2*v2y+h*ny;
		const double _z=z+x1*v1z+x2*v2z+h*nz;
		
		pos[3*i]=_x;
		pos[3*i+1]=_y;
		pos[3*i+2]=_z;
		
		phi[i]=p;
	}
}

void tubule::integrate(const double dt)
{
	for(unsigned int i=0; i<m; ++i)
	{
		const double _x=pos[3*i]-x;
		const double _y=pos[3*i+1]-y;
		const double _z=pos[3*i+2]-z;
		
		const double _h=_x*nx+_y*ny+_z*nz;
		const double _p=phi[i];
		
		double h=_h+gsl_ran_gaussian(rng,1.0)*sqrt(2*D1*dt);

		
		double p=_p+gsl_ran_gaussian(rng,1.0)*sqrt(2*D2*dt)/R;
		if(p<0) p+=2*M_PI;
		if(p>2*M_PI) p-=2*M_PI;
		
		const double a=R*cos(p);
		const double b=R*sin(p);
		
		const double xx=a*v1x+b*v2x+x+h*nx;
		const double yy=a*v1y+b*v2y+y+h*ny;
		const double zz=a*v1z+b*v2z+z+h*nz;
		
		pos[3*i]=xx;
		pos[3*i+1]=yy;
		pos[3*i+2]=zz;	
		
		phi[i]=p;
	}
}

