#include "image.hpp"

bool image::linescan(const double x, const double y, const double vx, const double vy,const unsigned int m, double *pos, const unsigned int frame)
{
	double _x=x-0.5*vx;
	double _y=y-0.5*vy;
	for(unsigned int i=0; i<iX; ++i)
	{
		_x+=vx;
		_y+=vy;
		pic_buffer[i+(iY-1-row)*iX]=compute_pixel(_x,_y,m,pos);
	}
	
	++row;
	
	if(row==iY)
	{
		row=0;
		char name[200];
		sprintf(name,"frame_%08d.tif",frame);
		buffer_to_image(name);
		return true;
	}
	return false;
}

double image::compute_pixel(const double x, const double y, const unsigned int m, double *pos)
{

	const double SNR=.5;
	static const double prefactor=1./sqrt(M_PI*Rz2/2.)*1./(M_PI*R2/2.);
	const double ampl=0.1/prefactor;
	const double sigma=ampl*prefactor/SNR;
	
	//printf("%lf %lf | %lf %lf\n",R2,Rz2,prefactor,sigma);
	
	double signal=0;
	
	double *p=pos;
		
	for(unsigned int i=0; i<m; ++i)
	{
		const double x0=*(p++);
		const double y0=*(p++);
		const double z0=*(p++);
		
		const double ax=(-2*(x-x0)*(x-x0)/(R2));			
		const double ay=(-2*(y-y0)*(y-y0)/(R2));
		const double az=(-2*(z0*z0)/(Rz2));
		
		
		const double s=exp(ax+ay+az);
		double _signal=ampl*prefactor*s;
		
		signal+=_signal;
	}
	signal+=gsl_rng_uniform(rng)*sigma;
	if (signal>1) { signal=1; fprintf(stderr,"too large\n"); fflush(stderr);}
	if (signal<0) signal=0;
	
	return signal;
}

void image::buffer_to_image(const char * name)
{
	printf("writing %s\n",name);
	TIFF *out= TIFFOpen(name, "w");
	
	int sampleperpixel = 1;
	
	TIFFSetField (out, TIFFTAG_IMAGEWIDTH, iX);  // set the width of the image
	TIFFSetField(out, TIFFTAG_IMAGELENGTH, iY);    // set the height of the image
	TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, sampleperpixel);   // set number of channels per pixel
	TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 16);    // set the size of the channels
	TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
	//   Some other essential fields to set that you do not have to understand for now.
	TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	//TIFFSetField(out, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE);
	
	tsize_t linebytes = sampleperpixel * iX;     // length in memory of one row of pixel in the image.

	uint16 * buf =(uint16 *)_TIFFmalloc(linebytes*sizeof(uint16));
	
	TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out, iX*sampleperpixel));

	//Now writing image to the file one strip at a time
	for (uint32 row = 0; row < iY; row++)
	{
		for(unsigned int q=0; q<iX; ++q)
		{
			for(unsigned int w=0; w<sampleperpixel; ++w)
			{
				buf[sampleperpixel*q+w]=pic_buffer[q+(iY-1-row)*iX]*65535;
			}
		}
		if (TIFFWriteScanline(out, buf, row, 0) < 0)
		break;
	}
	(void) TIFFClose(out);

	if (buf) _TIFFfree(buf);
}
