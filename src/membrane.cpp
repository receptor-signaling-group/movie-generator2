#include "membrane.hpp"

void membrane::initialize() 
{
	for(unsigned int i=0; i<m; ++i)
	{
		const double x1=L1*gsl_rng_uniform(rng);
		const double x2=L2*gsl_rng_uniform(rng);
		
		const double _x=x+x1*v1x+x2*v2x;
		const double _y=y+x1*v1y+x2*v2y;
		const double _z=z+x1*v1z+x2*v2z;
		
		pos[3*i]=_x;
		pos[3*i+1]=_y;
		pos[3*i+2]=_z;
		
		//printf("%lf %lf %lf | %lf %lf %lf | %lf %lf\n",_x,_y,_z,x,y,z,L1,L2);
	}
}

void membrane::integrate(const double dt)
{
	for(unsigned int i=0; i<m; ++i)
	{
		const double _x=pos[3*i]-x;
		const double _y=pos[3*i+1]-y;
		const double _z=pos[3*i+2]-z;
		
		const double _a=_x*v1x+_y*v1y+_z*v1z;
		const double _b=_x*v2x+_y*v2y+_z*v2z;
		
		double a=_a+gsl_ran_gaussian(rng,1.0)*sqrt(2*D*dt);
		if(a<0) a+=L1;
		if(a>L1) a-=L1;
		
		double b=_b+gsl_ran_gaussian(rng,1.0)*sqrt(2*D*dt);
		if(b<0) b+=L2;
		if(b>L2) b-=L2;
		
		//printf("%lf %lf %lf | %lf %lf %lf | %lf %lf %lf\n",_x,_y,_z,v1x,v1y,v1z,v2x,v2y,v2z);
		
		
		
		
		const double xx=a*v1x+b*v2x+x;
		const double yy=a*v1y+b*v2y+y;
		const double zz=a*v1z+b*v2z+z;
		
		//printf("\t%lf %lf | %lf %lf | %lf %lf : %lf %lf -> %lf %lf\n",a,b,_a,_b,L1,L2,_x,_z,x,z);	
		//printf("%lf %lf %lf %lf %lf | %lf %lf\n",pos[3*i]-xx,a-_a,b-_b,yy,zz,L1,L2);
		pos[3*i]=xx;
		pos[3*i+1]=yy;
		pos[3*i+2]=zz;	
	}
	//printf("\n\n");
	//exit(-1);
}
